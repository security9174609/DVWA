# Update package lists
sudo su
sudo apt update

# Install required packages
sudo apt install -y apache2 mariadb-server mariadb-client php php-mysqli php-gd libapache2-mod-php

sudo rm -rf DVWA/

# Clone DVWA (Damn Vulnerable Web Application) repository
git clone https://gitlab.com/security9174609/DVWA.git

# Remove existing contents in /var/www/ directory
sudo rm -rf /var/www/html/DVWA/

# Copy DVWA files to /var/www/ directory
sudo cp -r /home/ubuntu/DVWA /var/www/html/

# Navigate to DVWA directory
cd /var/www/html/DVWA

# Copy DVWA configuration file
cp config/config.inc.php.dist config/config.inc.php

# Restart Apache web server 
sudo service apache2 reload
